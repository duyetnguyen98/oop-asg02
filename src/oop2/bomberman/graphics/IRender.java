package oop2.bomberman.graphics;

public interface IRender {

	void update();
	
	void render(Screen screen);
}
