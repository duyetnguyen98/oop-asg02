package oop2.bomberman.level;

import oop2.bomberman.Board;
import oop2.bomberman.exceptions.LoadLevelException;
import oop2.bomberman.sound.Audio;

// Load v� l?u tr? th�ng tin b?n ?? c�c m�n ch?i
public abstract class LevelLoader {

	protected int _width = 20, _height = 20; // default values just for testing
	protected int _level;
	protected Board _board;
        
	public LevelLoader(Board board, int level) throws LoadLevelException {
		_board = board;
		loadLevel(level);
                //TODO: am thanh tro choi
                //Audio.playVictory();
                
	}

	public abstract void loadLevel(int level) throws LoadLevelException;

	public abstract void createEntities();

	public int getWidth() {
		return _width;
	}

	public int getHeight() {
		return _height;
	}

	public int getLevel() {
		return _level;
	}

}