package oop2.bomberman.entities.tile.item;

import oop2.bomberman.Game;
import oop2.bomberman.entities.Entity;
import oop2.bomberman.entities.character.Bomber;
import oop2.bomberman.graphics.Sprite;

public class FlameItem extends Item {

	public FlameItem(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public boolean collide(Entity e) {
		// TODO: xu ly Bomber an Item
                if(e instanceof  Bomber){
                    ((Bomber) e).addPowerup(this);
                    remove();
                    return true;
                }
		return false;
	}

    @Override
    public void setValues() {
        _active = true;
        Game.addBombRadius(1);
    }

}
