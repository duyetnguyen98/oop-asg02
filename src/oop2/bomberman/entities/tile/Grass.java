package oop2.bomberman.entities.tile;

import oop2.bomberman.entities.Entity;
import oop2.bomberman.graphics.Sprite;

public class Grass extends Tile {

	public Grass(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	/**
	 * Cho bat k� doi tuong kh�c di qua
	 * @param e
	 * @return
	 */
	@Override
	public boolean collide(Entity e) {
		return true;
	}
}
