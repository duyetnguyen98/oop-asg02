package oop2.bomberman.entities.tile;

import oop2.bomberman.entities.Entity;
import oop2.bomberman.graphics.Screen;
import oop2.bomberman.graphics.Sprite;
import oop2.bomberman.level.Coordinates;

/**
 * Entity co dinh, kh�ng di chuyen
 */
public abstract class Tile extends Entity {
	
	public Tile(int x, int y, Sprite sprite) {
		_x = x;
		_y = y;
		_sprite = sprite;
	}

	/**
	 * Mac dinh kh�ng cho bat cu mot doi tuong n�o di qua
	 * @param e
	 * @return
	 */
	@Override
	public boolean collide(Entity e) {
		return false;
	}
	
	@Override
	public void render(Screen screen) {
		screen.renderEntity( Coordinates.tileToPixel(_x), Coordinates.tileToPixel(_y), this);
	}
	
	@Override
	public void update() {}
}