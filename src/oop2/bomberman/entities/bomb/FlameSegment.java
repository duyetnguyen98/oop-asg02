package oop2.bomberman.entities.bomb;

import oop2.bomberman.entities.Entity;
import oop2.bomberman.graphics.Screen;
import oop2.bomberman.graphics.Sprite;
import oop2.bomberman.entities.character.Character;

public class FlameSegment extends Entity {

	protected boolean _last;

	/*
	 * @param x
	 * @param y
	 * @param direction
	 * @param last cho biet segment n�y l� cuoi c�ng cua Flame hay kh�ng segment cuoi c� sprite kh�c so voi c�c segment c�n lai
	 */
	
        public FlameSegment(int x, int y, int direction, boolean last) {
		_x = x;
		_y = y;
		_last = last;

		switch (direction) {
			case 0:
				if(!last) {
					_sprite = Sprite.explosion_vertical2;
				} else {
					_sprite = Sprite.explosion_vertical_top_last2;
				}
			break;
			case 1:
				if(!last) {
					_sprite = Sprite.explosion_horizontal2;
				} else {
					_sprite = Sprite.explosion_horizontal_right_last2;
				}
				break;
			case 2:
				if(!last) {
					_sprite = Sprite.explosion_vertical2;
				} else {
					_sprite = Sprite.explosion_vertical_down_last2;
				}
				break;
			case 3: 
				if(!last) {
					_sprite = Sprite.explosion_horizontal2;
				} else {
					_sprite = Sprite.explosion_horizontal_left_last2;
				}
				break;
		}
	}
	
	@Override
	public void render(Screen screen) {
		int xt = (int)_x << 4;
		int yt = (int)_y << 4;
		
		screen.renderEntity(xt, yt , this);
	}
	
	@Override
	public void update() {}

	@Override
	public boolean collide(Entity e) {
		// TODO: xu l� khi FlameSegment va cham voi Character
                if(e instanceof Character){
                    ((Character) e).kill();
                }
		return true;
	}
	

}